function startDiagnostic() {
	let travInput = prompt("In the past 14 days, have you travelled to countries with local COVID transmissions? (Y/N)");

	if (travInput.toLowerCase() == "n" || travInput.toLowerCase() == "no") {
		let travInput2 = prompt("Does at least 1 of the following criteria apply to you? (Y/N) \n A. You have provided direct care to a COVID-19 patient \n B. You have worked together with or stayed in the same close environment to a COVID-a0 patient \n C. You have travelled together with a COVID-19 patient \n D. You have lived in the same household as a COVID-19 patient in the last 14 days \n y/n");

		if (travInput2.toLowerCase() == "n" || travInput2.toLowerCase() == "no") {
			alert("You are neither a Person Under Monitoring or a Person Under Investigation");

		} else if (travInput2.toLowerCase() == "y" || travInput2.toLowerCase() == "yes") {
			let travInput3 = prompt("Do you have at least one of the following symptoms? (Y/N) \n A. A fever of at least 38 degrees Celcius \n B. A respiratory illness \n C. A cough \n D. A cold")
			if (travInput3.toLowerCase() == "n" || travInput3.toLowerCase() == "no") {
				alert("You are a Person Under Monitoring. Practice home quarantine. STAY AT HOME!")
			} else if (travInput3.toLowerCase() == "y" || travInput3.toLowerCase() == "yes") {
				alert("You are a Patient Under Investigation. Please refer to the nearest hospital. GO TO THE HOSPITAL!")
			} else {
				alert("Please enter a valid answer (y/yes or n/no)")
			}
		} else {
			alert("Please enter a valid answer (y/yes or n/no)")
		}
	} else if (travInput.toLowerCase() == "y" || travInput.toLowerCase() == "yes") {
		let travInput3 = prompt("Do you have at least one of the followign symptoms? (Y/N) \n A. A fever of at least 38 degrees Celcius \n B. A respiratory illness \n C. A cough \n D. A cold")
		if (travInput3.toLowerCase() == "n" || travInput3.toLowerCase() == "no") {
			alert("You are a Person Under Monitoring. Practice home quarantine. STAY AT HOME!")
		} else if (travInput3.toLowerCase() == "y" || travInput3.toLowerCase() == "yes") {
			alert("You are a Patient Under Investigation. Please refer to the nearest hospital. GO TO THE HOSPITAL! \n ")
		} else {
			alert("Please enter a valid answer (y/yes or n/no)")
		}
	} else {
		alert("Please enter a valid answer (y/yes or n/no)")
	}

}