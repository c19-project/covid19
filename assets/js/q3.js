document.querySelector("#q3btn").addEventListener("click", () => {

let q3 = document.querySelector("#q3").value;

	if(q3.toLowerCase() == "yes") {
		document.querySelector("#q3Err").innerHTML = `<small class="text-secondary"></small>`;
		localStorage.setItem("q3", "yes");
		window.location.href="./profile.html"
	}
	else if(q3.toLowerCase() == "no") {
		document.querySelector("#q3Err").innerHTML = `<small class="text-secondary"></small>`;
		localStorage.setItem("q3", "no");
		window.location.href="./profile.html"
	}
	else {
		document.querySelector("#q3Err").innerHTML = `<small class="alert-danger">Just type "yes" or "no"</small>`;
	}

})