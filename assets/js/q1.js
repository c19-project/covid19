const Toast = Swal.mixin({
	toast : true,
	position : 'top-end',
	showConfirmButton : false,
	timer : 3000
})

document.querySelector("#q1btn").addEventListener("click", () => {


	let q1 = document.querySelector("#q1").value;

	if(q1.toLowerCase() == "yes") {
		document.querySelector("#q1Err").innerHTML = `<small class="text-secondary"></small>`;
		localStorage.setItem("Traveller", "A traveller");
		window.location.href="./q2.html"
	}
	else if(q1.toLowerCase() == "no") {
		document.querySelector("#q1Err").innerHTML = `<small class="text-secondary"></small>`;
		localStorage.setItem("Traveller", "Not a traveller");
		window.location.href="./q2.html"
	}
	else {
		document.querySelector("#q1Err").innerHTML = `<small class="alert-danger">Just type "yes" or "no"</small>`;
	}

} )